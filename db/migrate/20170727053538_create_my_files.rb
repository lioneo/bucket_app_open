class CreateMyFiles < ActiveRecord::Migration
  def change
    create_table :my_files do |t|
      t.string :local_file
      t.string :s3_file
      t.binary :db_file
      t.belongs_to :user, index: true, foreign_key: true
      # rails lock_version
      t.integer :lock_version, default: 0

      t.timestamps null: false
    end
  end
end
