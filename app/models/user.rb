class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  VALID_PHONE_NUM_REGEX = /01[0|1|6|7|8|9]{1}(\d{4})(\d{4})/
  validates :phone_number, {
      presence: true,
      length: { is: 11 },
      uniqueness: true,
      format: { with: VALID_PHONE_NUM_REGEX }
  }
  
  has_many :my_files
end
