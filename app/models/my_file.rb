class MyFile < ActiveRecord::Base
  mount_uploader :local_file, LocalUploader
  mount_uploader :s3_file, S3Uploader

  belongs_to :user

  validates :user_id, presence: true

end
