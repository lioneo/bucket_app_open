class MyFilesController < ApplicationController
  before_action :set_my_file, only: [:show, :edit, :update, :destroy]
  def new
    @my_file = MyFile.new
  end

  def create
    input_file = my_file_params[:input_file]

    @my_file = MyFile.new
    @my_file.db_file = Base64.encode64 input_file.read
    @my_file.s3_file = @my_file.local_file = input_file
    @my_file.user_id = my_file_params[:user_id]
    @my_file.save
    redirect_to @my_file

  end

  def show
  end

  def index
    @my_files = MyFile.all
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_my_file
      @my_file = MyFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def my_file_params
      params.require(:my_file).permit(:input_file, :user_id)
    end
end
